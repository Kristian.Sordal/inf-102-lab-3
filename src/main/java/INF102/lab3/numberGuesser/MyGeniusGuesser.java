package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int lo = number.getLowerbound();
        int hi = number.getUpperbound();
        int mid = 0;
        int correct = 0;

        while (lo <= hi) {
            mid = hi - (hi - lo) / 2;
            correct = number.guess(mid);

            if (correct == -1) {
                lo = mid + 1;
            } else if (correct == 1) {
                hi = mid - 1;
            } else {
                break;
            }
        }
        return mid;
    }
}
