package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        int size = numbers.size();

        if (numbers.isEmpty()) {
            return 0;
        }

        if (size == 1) {
            return numbers.get(0);
        } else if (size == 2) {
            int first = numbers.get(0);
            int second = numbers.get(1);
            if (first >= second)
                return first;
            return second;
        } else if (size >= 3) {
            int first = numbers.get(0);
            int second = numbers.get(1);
            int third = numbers.get(2);

            if (first >= second)
                return first;
            else if (second >= first && second >= third)
                return second;
        }
        return peakElement(numbers.subList(1, size));
    }
}
