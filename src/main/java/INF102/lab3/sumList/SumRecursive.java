package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        int size = list.size();

        if (list.isEmpty())
            return 0;

        if (size == 1)
            return list.get(0);

        list.set(0, (list.get(0) + list.remove(size - 1)));

        return sum(list);
    }
}
